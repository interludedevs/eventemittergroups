#!/bin/bash
node_modules/.bin/uglifyjs\
    --comments\
    --mangle sort=true\
    --compress\
    --output EventEmitterGroups.min.js EventEmitterGroups.js
