# EventEmitterGroups

## An Event Emitter with grouping of event handlers

This is a fork of Oliver Caldwell's [EventEmitter].

It extends the well-known EventEmitter by allowing you to gather event-handlers together into groups.  Rather than namespacing the events, this event emitter namespaces the handlers, which makes more sense.

This allows for easy cleanup and manual garbage collection of events handlers.
 
## Dependencies

There are no hard dependencies. The only reason you will want to run `npm install` to grab the development dependencies is to build the documentation or minify the source code. No other scripts are required to actually use EventEmitterGroups.

## Documentation

 * [Guide][]
 * [API][]

### Examples

These examples only show the original Event Emitter functionality

 * [Simple][]
 * [RegExp DOM caster][]

## Contributing (aim your pull request at the `develop` branch!)

If you wish to contribute to the project then please commit your changes into the `develop` branch. All pull requests should contain a failing test which is then resolved by your additions. [A perfect example][example] was submitted by [nathggns][].

## Testing

Tests are performed using [Mocha][] and [Chai][], just serve up the directory using your local HTTP server of choice ([http-server][] is probably a good choice) and open up `tests/index.html`. You can also use the server scripts in the `tools` directory.

## Building the documentation

You can run `tools/doc.sh` to build from the JSDoc comments found within the source code. The built documentation will be placed in `docs/api.md`. I actually keep this inside the repository so each version will have it's documentation stored with it.

## Minifying

You can grab minified versions of EventEmitterGroups from inside this repository, every version is tagged. If you need to build a custom version then you can run `tools/dist.sh`.

## Cloning

You can clone the repository with your generic clone commands as a standalone repository or submodule.

```bash
# Full repository
git clone git://bitbucket.org/interludedevs/eventemittergroups.git

# Or submodule
git submodule add git://bitbucket.org/interludedevs/eventemittergroups.git assets/js/EventEmitterGroups
```

### Package managers

You can also get a copy of EventEmitterGroups through the following package managers:
 * [NPM][] (EventEmitterGroups)
 * [Bower][] (EventEmitterGroups)
 * [Component][] (EventEmitterGroups)

## Unlicense

This project is released under the [Unlicense][]. Here's the gist of it but you can find the full thing in the `UNLICENSE` file.

>This is free and unencumbered software released into the public domain.
>
>Anyone is free to copy, modify, publish, use, compile, sell, or distribute this software, either in source code form or as a compiled binary, for any purpose, commercial or non-commercial, and by any means.



[guide]: https://bitbucket.org/interludedevs/eventemittergroups/src/master/docs/guide.md
[api]: https://bitbucket.org/interludedevs/eventemittergroups/src/master/docs/api.md
[simple]: http://jsfiddle.net/Wolfy87/qXQu9/
[regexp dom caster]: http://jsfiddle.net/Wolfy87/JqRvS/
[npm]: https://npmjs.org/
[bower]: http://bower.io/
[component]: http://github.com/component/component
[mocha]: http://visionmedia.github.io/mocha/
[chai]: http://chaijs.com/
[example]: https://bitbucket.org/interludedevs/EventEmitterGroups/pull/46
[nathggns]: https://github.com/nathggns
[http-server]: https://www.npmjs.org/package/http-server
[node.js]: http://nodejs.org/
[unlicense]: http://unlicense.org/
[EventEmitter]: https://github.com/Olical/EventEmitter
