# API

Each of the methods listed in the API are accessed through an instance of EventEmitterGroups. You can create an instance with `var ee = new EventEmitterGroups();`. Then you can call API methods from `ee`, for example `ee.emitEvent('foo');`.

You may also be interested in [the guide](https://bitbucket.org/interludedevs/eventemittergroups/src/master/docs/guide.md) which highlights some key features of EventEmitterGroups and how to use them. It is a broad overview of the script whereas this is concise information about each method in the API.

## EventEmitterGroups

Class for managing events.
Can be extended to provide event functionality in other classes.

 * **class** - [object Object]

## getListeners

Returns the listener array for the specified event.
Will initialise the event object and listener arrays if required.
Will return an object if you use a regex search. The object contains keys for each matched event. So /ba[rz]/ might return an object containing bar and baz. But only if you have either defined them with defineEvent or added some listeners to them.
Each property in the object response is an array of listener functions.

 * **param** (`String` | `RegExp`) _evt_ - Name of the event to return the listeners from.
 * **return** (`Array.&lt;Function&gt;` | `Object`) - All listener functions for the event.

## flattenListeners

Takes a list of listener objects and flattens it into a list of listener functions.

 * **param** (`Array.&lt;Object&gt;`) _listeners_ - Raw listener objects.
 * **return** (`Array.&lt;Function&gt;`) - Just the listener functions.

## getListenersAsObject

Fetches the requested listeners via getListeners but will always return the results inside an object. This is mainly for internal use but others may find it useful.

 * **param** (`String` | `RegExp`) _evt_ - Name of the event to return the listeners from.
 * **return** (`Object`) - All listener functions for an event in an object.

## addListener

Adds a listener function to the specified event.
The listener will not be added if it is a duplicate.
If the listener returns true then it will be removed after it is called.
If you pass a regular expression as the event name then the listener will be added to all events that match it.

 * **param** (`String` | `RegExp`) _evt_ - Name of the event to attach the listener to.
 * **param** (`Function`) _listener_ - Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
 * **param** (`String`) _listenerGroup_ - OPTIONAL. Name of listenerGroup for this listener.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## on

Alias of addListener


## addOnceListener

Semi-alias of addListener. It will add a listener that will be
automatically removed after its first execution.

 * **param** (`String` | `RegExp`) _evt_ - Name of the event to attach the listener to.
 * **param** (`Function`) _listener_ - Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
 * **param** (`String`) _listenerGroup_ - OPTIONAL. Name of listenerGroup for this listener.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## once

Alias of addOnceListener.


## defineEvent

Defines an event name. This is required if you want to use a regex to add a listener to multiple events at once. If you don't do this then how do you expect it to know what event to add to? Should it just add to every possible match for a regex? No. That is scary and bad.
You need to tell it what event names should be matched by a regex.

 * **param** (`String`) _evt_ - Name of the event to create.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## defineEvents

Uses defineEvent to define multiple events.

 * **param** (`Array.&lt;String&gt;`) _evts_ - An array of event names to define.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## removeListener

Removes a listener function from the specified event.
When passed a regular expression as the event name, it will remove the listener from all events that match it.

 * **param** (`String` | `RegExp`) _evt_ - Name of the event to remove the listener from.
 * **param** (`Function`) _listener_ - Method to remove from the event.
 * **param** (`String`) _listenerGroup_ - OPTIONAL. Name of listenerGroup to remove.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## off

Alias of removeListener


## addListeners

Adds listeners in bulk using the manipulateListeners method.
If you pass an object as the second argument you can add to multiple events at once. The object should contain key value pairs of events and listeners or listener arrays. You can also pass it an event name and an array of listeners to be added.
You can also pass it a regular expression to add the array of listeners to all events that match it.
Yeah, this function does quite a bit. That's probably a bad thing.

 * **param** (`String` | `Object` | `RegExp`) _evt_ - An event name if you will pass an array of listeners next. An object if you wish to add to multiple events at once.
 * **param** (`Array.&lt;Function&gt;`) _[listeners]_ - An optional array of listener functions to add.
 * **param** (`String`) _listenerGroup_ - OPTIONAL. Name of listenerGroup for these listeners.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## removeListeners

Removes listeners in bulk using the manipulateListeners method.
If you pass an object as the second argument you can remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
You can also pass it an event name and an array of listeners to be removed.
You can also pass it a regular expression to remove the listeners from all events that match it.
If you pass a listenerGroup, then you don't need to pass specific listeners.

 * **param** (`String` | `Object` | `RegExp`) _evt_ - An event name if you will pass an array of listeners next. An object if you wish to remove from multiple events at once.
 * **param** (`Array.&lt;Function&gt;`) _[listeners]_ - An optional array of listener functions to remove.
 * **param** (`String`) _listenerGroup_ - OPTIONAL. Remove only listeners which belong to this group.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## manipulateListeners

Edits listeners in bulk. The addListeners and removeListeners methods both use this to do their job. You should really use those instead, this is a little lower level.
The first argument will determine if the listeners are removed (true) or added (false).
If you pass an object as the second argument you can add/remove from multiple events at once. The object should contain key value pairs of events and listeners or listener arrays.
You can also pass it an event name and an array of listeners to be added/removed.
You can also pass it a regular expression to manipulate the listeners of all events that match it.

 * **param** (`Boolean`) _remove_ - True if you want to remove listeners, false if you want to add.
 * **param** (`String` | `Object` | `RegExp`) _evt_ - An event name if you will pass an array of listeners next. An object if you wish to add/remove from multiple events at once.
 * **param** (`Array.&lt;Function&gt;`) _[listeners]_ - An optional array of listener functions to add/remove.
 * **param** (`String`) _listenerGroup_ - OPTIONAL. Name of listenerGroup of added/removed listeners.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## removeEvent

Removes all listeners from a specified event.
If you do not specify an event then all listeners will be removed.
That means every event will be emptied.
You can also pass a regex to remove all events that match it.

 * **param** (`String` | `RegExp`) _[evt]_ - Optional name of the event to remove all listeners for. Will remove from every event if not passed.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## removeAllListeners

Alias of removeEvent.

Added to mirror the node API.


## emitEvent

Emits an event of your choice.
When emitted, every listener attached to that event will be executed.
If you pass the optional argument array then those arguments will be passed to every listener upon execution.
Because it uses `apply`, your array of arguments will be passed as if you wrote them out separately.
So they will not arrive within the array on the other side, they will be separate.
You can also pass a regular expression to emit to all events that match it.

 * **param** (`String` | `RegExp`) _evt_ - Name of the event to emit and execute listeners for.
 * **param** (`Array`) _[args]_ - Optional array of arguments to be passed to each listener.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## trigger

Alias of emitEvent


## emit

Subtly different from emitEvent in that it will pass its arguments on to the listeners, as opposed to taking a single array of arguments to pass on.
As with emitEvent, you can pass a regex in place of the event name to emit to all events that match it.

 * **param** (`String` | `RegExp`) _evt_ - Name of the event to emit and execute listeners for.
 * **param** _Optional_ - additional arguments to be passed to each listener.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## setOnceReturnValue

Sets the current value to check against when executing listeners. If a
listeners return value matches the one set here then it will be removed
after execution. This value defaults to true.

 * **param** _value_ - The new value to check for when executing listeners.
 * **return** (`Object`) - Current instance of EventEmitterGroups for chaining.

## noConflict

Reverts the global {@link EventEmitterGroups} to its previous value and returns a reference to this version.

 * **return** (`Function`) - Non conflicting EventEmitterGroups class.

